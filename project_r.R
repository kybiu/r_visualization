library(shiny)
library(DT)
library(ggplot2)
library(dplyr)
library(corrplot)
library(RColorBrewer)

mushroom <- read.csv(file ="data/mushrooms.csv")

mushroom[sapply(mushroom, is.character)] <- lapply(mushroom[sapply(mushroom, is.character)], as.factor)

levels(mushroom$class) <- c("edible", "poisonous")
levels(mushroom$cap.shape) <- c("bell", "conical", "flat", "knobbed", "sunken", "convex")
levels(mushroom$cap.color) <- c("buff", "cinnamon", "red", "gray", "brown", "pink", "green", "purple", "white", "yellow")
levels(mushroom$cap.surface) <- c("fibrous", "grooves", "scaly", "smooth")
levels(mushroom$bruises) <- c("no", "yes")
levels(mushroom$odor) <- c("almond", "creosote", "foul", "anise", "musty", "none", "pungent", "spicy", "fishy")
levels(mushroom$gill.attachment) <- c("attached", "free")
levels(mushroom$gill.spacing) <- c("close", "crowded")
levels(mushroom$gill.size) <- c("broad", "narrow")
levels(mushroom$gill.color) <- c("buff", "red", "gray", "chocolate", "black", "brown", "orange", "pink", "green", "purple", "white", "yellow")
levels(mushroom$stalk.shape) <- c("enlarging", "tapering")
levels(mushroom$stalk.root) <- c("missing", "bulbous", "club", "equal", "rooted")
levels(mushroom$stalk.surface.above.ring) <- c("fibrous", "silky", "smooth", "scaly")
levels(mushroom$stalk.surface.below.ring) <- c("fibrous", "silky", "smooth", "scaly")
levels(mushroom$stalk.color.above.ring) <- c("buff", "cinnamon", "red", "gray", "brown", "pink", "green", "purple", "white", "yellow")
levels(mushroom$stalk.color.below.ring) <- c("buff", "cinnamon", "red", "gray", "brown", "pink", "green", "purple", "white", "yellow")
levels(mushroom$veil.type) <- "partial"
levels(mushroom$veil.color) <- c("brown", "orange", "white", "yellow")
levels(mushroom$ring.number) <- c("none", "one", "two")
levels(mushroom$ring.type) <- c("evanescent", "flaring", "large", "none", "pendant")
levels(mushroom$spore.print.color) <- c("buff", "chocolate", "black", "brown", "orange", "green", "purple", "white", "yellow")
levels(mushroom$population) <- c("abundant", "clustered", "numerous", "scattered", "several", "solitary")
levels(mushroom$habitat) <- c("wood", "grasses", "leaves", "meadows", "paths", "urban", "waste")

#UI function
ui <- fluidPage(
  
  # App title
  titlePanel("Project Nguyen Duc Anh"),
  
  navbarPage("Champignons",
             
             # ACCUEIL
             tabPanel("Accueil",
                      tags$div(
                        tags$p("L'ensemble de données a été tiré. Il catégorise les champignons selon une série de caractéristiques, y compris les détails du chapeau, des branchies, de la tige et des anneaux. Il enregistre également s'ils sont comestibles ou toxiques."),
                      ),
                      
                      tags$br(),
                      
                      tags$table(
                        tags$td(
                          tags$ul(
                            tags$li("Nombre d'individus : 8124"),
                            tags$li("Nombre de variables : 23 qualitatives",
                                    tags$ul(
                                      tags$li("class: edible/poisonous"),
                                      tags$li("cap-shape: bell=b,conical=c,convex=x,flat=f, knobbed=k,sunken=s"),
                                      tags$li("cap-surface: fibrous=f,grooves=g,scaly=y,smooth=s"),
                                      tags$li("cap-color: brown=n,buff=b,cinnamon=c,gray=g,green=r,pink=p,purple=u,red=e,white=w,yellow=y"),
                                      tags$li("bruises: bruises=t,no=f"),
                                      tags$li("odor: almond=a,anise=l,creosote=c,fishy=y,foul=f,musty=m,none=n,pungent=p,spicy=s"),
                                      tags$li("gill-attachment: attached=a, descending=d, free=f, notched=n"),
                                      tags$li("gill-spacing: close=c,crowded=w,distant=d"),
                                      tags$li("gill-size: broad=b,narrow=n"),
                                      tags$li("gill-color: black=k,brown=n,buff=b,chocolate=h,gray=g, green=r,orange=o,pink=p,purple=u,red=e,white=w,yellow=y"),
                                      tags$li("stalk-shape: enlarging=e,tapering=t"),
                                      tags$li("stalk-root: bulbous=b,club=c,cup=u,equal=e,rhizomorphs=z,rooted=r,missing=m"),
                                      tags$li("stalk-surface-above-ring: fibrous=f,scaly=y,silky=k,smooth=s"),
                                      tags$li("stalk-surface-below-ring: fibrous=f,scaly=y,silky=k,smooth=s"),
                                      tags$li("stalk-color-above-ring: brown=n,buff=b,cinnamon=c,gray=g,orange=o,pink=p,red=e,white=w,yellow=y"),
                                      tags$li("stalk-color-below-ring: brown=n,buff=b,cinnamon=c,gray=g,orange=o,pink=p,red=e,white=w,yellow=y"),
                                      tags$li("veil-type: partial=p,universal=u"),
                                      tags$li("veil-color: brown=n,orange=o,white=w,yellow=y"),
                                      tags$li("ing-number: none=n,one=o,two=t"),
                                      tags$li("ring-type: cobwebby=c,evanescent=e,flaring=f,large=l,none=n,pendant=p,sheathing=s,zone=z"),
                                      tags$li("spore-print-color: black=k,brown=n,buff=b,chocolate=h,green=r,orange=o,purple=u,white=w,yellow=y"),
                                      tags$li("population: abundant=a,clustered=c,numerous=n,scattered=s,several=v,solitary=y"),
                                      tags$li("habitat: grasses=g,leaves=l,meadows=m,paths=p,urban=u,waste=w,woods=d"),
                                      
                                    ),
                            ),
                          ),
                        ),
                        tags$td(tags$span(style="color:white", "XXXXX")),
                        tags$td(img(src='./draw-amanita.jpg', align = "right", width = "300px", height = "300px")),
                        
                      ),
                        
                      
                      tags$br(),
                      
                      tags$div(
                        tags$p(tags$b("Data Sources :")),
                        tags$a("https://www.kaggle.com/uciml/mushroom-classification"),
                      ),
                      
                      tags$br(),
             ),
             
             # BOITES A MOUSTACHES
             tabPanel("Boxplot",
                      sidebarLayout(
                        sidebarPanel(
                          selectInput('zcol', 'Variable', names(mushroom[-1])),
                        ),
                        mainPanel(
                          plotOutput("boxplot")
                        )
                      )
             ),
             
             # SCATTER PLOT
             tabPanel("Scatter plot",
                      sidebarLayout(
                        
                        sidebarPanel(
                          selectInput('xcol', 'Variable 1(x-axis)', names(mushroom), selected="odor"),
                          selectInput('ycol', 'Variable 2(y-axis)', names(mushroom), selected="class"),
                        ),
                        
                        mainPanel("Scatter plot",
                          plotOutput("scatter_plot")
                        ),
                      )
              ),
             
             # BAR GRAPHE
             tabPanel("Bar Chart",
                      sidebarLayout(
                        sidebarPanel(
                          selectInput('hcol', 'Variable', names(mushroom)),
                          checkboxInput(inputId = "is_stacked",
                                        label = strong("Grouped bar chart"),
                                        value = FALSE),
                        ),
                        
                        mainPanel(
                          plotOutput(outputId = "bar_chart"),
                        )
                      )
             ),
             
             # CORRELATION
             tabPanel("Corrélation",
                      sidebarLayout(
                        sidebarPanel(
                          selectInput('class', 'Mushroom class', c("edible", "poisonous"), selected = 1)
                        ),
                        mainPanel(
                          plotOutput("correlation")
                        )
                      ),
             ),
             
             
             # STATISTIQUES DESCRIPTIVES
             tabPanel("Statistiques descriptives",
                      verbatimTextOutput("summary")
             ),
             
             # DATA
             tabPanel("Données",
                      DT::dataTableOutput("bTable")
             ),
    
  )
)


#Server function
server <- function(input, output){
  
  #Output plot for Scatter plot tab
  select.2var <- reactive({
    res <- mushroom[, c(input$xcol, input$ycol)]
    res
  })
  
  output$scatter_plot <- renderPlot({
    data <- select.2var()
    mushroom_class <-  as.factor(mushroom$class)

    ggplot(data = data, aes(x = data[,1] , y= data[,2], color = mushroom_class)) +
      labs(title = "Scatter plot title\n", x = colnames(data)[1] , y = colnames(data)[2], color = "Mushroom class") +
      geom_point(position = "jitter") +
      scale_color_manual(labels = c("edible", "poisonous"), breaks = c("edible", "poisonous"), 
                         values = c("green", "red"))
  })
  
  #Output plot for boxplot tab
  select.1var <- reactive({
    res <- mushroom[,input$zcol]
    res
  })
  
  output$boxplot <- renderPlot({
    data <- select.1var()
    data_x <- as.numeric(factor(data))

    glimpse(data)
    glimpse(data_x)
    ggplot(mushroom, aes(x = mushroom$class , y= data_x)) +
      geom_boxplot() +
      labs(x ="Mushroom class", y = colnames(data)[1]) + 
      theme_minimal ()
  })
  
  #Output plot for bar chart tab
  mycolors = c(brewer.pal(name="Dark2", n = 8), brewer.pal(name="Paired", n = 6))
  
  select.1var.h <- reactive({
    res <- mushroom[,input$hcol]
    res
  })
  
  output$bar_chart <- renderPlot({
    data <- select.1var.h()

    if (input$is_stacked) {
      data_df <- data.frame(table(as.factor(mushroom$class), as.factor(data)))
      ggplot(data = data_df, aes(x = Var2 , y = Freq, fill = Var1)) +
        labs(title = "Bar plot\n" ,x="Value", y = "Count", color = "Mushroom class") +
        geom_bar(stat = "identity", position="dodge") +
        geom_text(aes(label=Freq), position=position_dodge(width=0.9), vjust=-0.25) +
        scale_fill_manual(name ='Mushroom class',labels = c("edible", "poisonous"), breaks = c("edible", "poisonous"), values = c("green", "red"))
    
      }else{
      
      data_df <- data.frame(table(as.factor(data)))
      ggplot(data = data_df, aes(x = data_df[,1] , y = data_df[,2], fill = data_df[,1])) +
        labs(title = "Bar plot\n", y = "Count") +
        geom_bar(stat = "identity") +
        geom_text(aes(label=data_df[,2]), position=position_dodge(width=0.9), vjust=-0.25)+
        scale_color_manual(mycolors) +
        theme(legend.position="none")
    }
    
  })
  
  # Output correlation
  select.mushroom_class <- reactive({
    res <- input$class
    res
  })
  data_x <- data.frame(sapply(mushroom[2:23], function (x) as.numeric(as.factor(x))))
  mushroom_x <- data.frame(data_x, class = mushroom$class)
  output$correlation <- renderPlot({
    selected_mushroom_class <- select.mushroom_class()
    idx <- which(mushroom_x$class==selected_mushroom_class)
    mat.cor <- cor(x = mushroom_x[idx,-23])
    corrplot(mat.cor, method="number",  col = COL2('BrBG', 10))
  }, width = 800, height = 800)
  
  # Output for STATISTIQUES DESCRIPTIVES tab
  output$summary <- renderPrint({summary(mushroom)})
  
  #Output table for data tab
  output$bTable <- DT::renderDataTable({mushroom})
}


# Create Shiny app ----
shinyApp(ui = ui, server = server)